"""
MultiGit

Git manager to use git as a cloud tool.

"""

import os
import sys
import time

USER = ""
PASSWORD = ""

    
class gitManager(object):

  def __init__(self, path='.'):

    self.returnPath = os.getcwd()


  def repo(self):
    
    os.chdir(self.path)
    os.popen("git init");
    os.chdir(self.returnPath)

  def push(self):

    os.chdir(self.path)
    os.popen("git push origin master");
    os.chdir(self.returnPath)
  
  def pull(self):
 
    os.chdir(self.path)
    os.popen("git pull origin master");
    os.chdir(self.returnPath)

  def commit(self):

    os.chdir(self.path)
    os.popen("git add .");
    os.popen("git commit -m '%s'" % str(time.time()));
    os.chdir(self.returnPath)


class gitIgnore(gitManager):

  def __init__(self, path="."):
    
    self.path = path

    super(self.__class__, self).__init__(path)

    self.gitIgnoreFile = open("%s/.gitignore" % self.path, "r+")
    
    if self.gitIgnoreFile.fileno:
      self.ignores = self.gitIgnoreFile.read()
    else:
      self.ignores = ""

  def ignore(self, rule):
    
    NewRule = "\n%s" % rule
    
    self.ignores += NewRule
    self.gitIgnoreFile.write(NewRule);


class bitBucketManager(gitManager):

  def __init__(self, path=".", user=USER, password=PASSWORD):
    
    super(self.__class__, self).__init__(path)
    
    self.path = path 
    self.name = path.split('/')[-1]

  def newRepo(self, name=''):
    
    if name:
      self.name = name
    
    Call = "curl --user %s:%s \
        https://api.bitbucket.org/1.0/repositories/ \
        --data name='%s'" % (self.user, self.password, self.name)



class multiGit():

  def __init__(self):
    pass



if __name__ == "__main__":
    
  Path = '.'

  Ignores = gitIgnore(Path)
  Ignores.ignore(".*.swp")
  
  New = bitBucketManager(Path)
  New.commit()
  New.push()


